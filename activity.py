
def is_leap_year(year):
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        return True
    else:
        return False


def get_integer_input(prompt):
    while True:
        try:
            value = int(input(prompt))
            if value <= 0:
                print("No zero or negative values.")
            else:
                return value
        except ValueError:
            print("Strings are not allowed for inputs")


year = get_integer_input("Please input a year: \n")
if is_leap_year(year):
    print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")

#############################################################################
row = get_integer_input("Please enter the number of rows: \n")
col = get_integer_input("Please enter the number of columns: \n")


for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()  
